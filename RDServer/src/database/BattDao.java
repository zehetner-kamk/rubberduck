package database;


import database.domainObjects.BattDO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class BattDao {
    private static final String TABLE_NAME = "batt";

    public boolean insert(BattDO batt) {
        String insertStatement = "INSERT INTO " + TABLE_NAME + " (duckid, time, batt) VALUES (?, ?, ?)";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(insertStatement)) {
                statement.setInt(1, batt.getDuckID());
                statement.setTimestamp(2, new Timestamp(batt.getTime()));
                statement.setDouble(3, batt.getBatt());

                statement.execute();
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public List<BattDO> getByDuckID(int duckid) {
        String insertStatement = "SELECT time, batt FROM " + TABLE_NAME + " WHERE duckid=(?)";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(insertStatement)) {
                statement.setInt(1, duckid);

                ResultSet result = statement.executeQuery();
                List<BattDO> batt = new ArrayList<>();
                while (result.next()) {
                    BattDO b = new BattDO(duckid, result.getTimestamp(1).getTime(), result.getDouble(2));
                    batt.add(b);
                }

                return batt;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new ArrayList<>();
    }

}

package database;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

public class ConnectionManager {
    private static final String CONFIG_LOCATION = "/opt/app-root/config/"; //location of the config files
    private static final String DB_NAME = readConfig(CONFIG_LOCATION + "database-name");
    private static final String USERNAME = readConfig(CONFIG_LOCATION + "database-user");
    private static final String PASSWORD = readConfig(CONFIG_LOCATION + "database-password");
    private static Connection connection;

    static {
        try {
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static Connection getConnection() {
        try {
            if (connection == null || connection.isClosed()) { //the db url couldn't be retrieved from a file, therefore it needs to be adapted here
                connection = DriverManager.getConnection("jdbc:mysql://mysql.rubberduck-kamk.svc:3306/" + DB_NAME, USERNAME, PASSWORD);
            }
            return connection;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null; //could not create connection
    }

    private static String readConfig(String filename) {
        try {
            Scanner s = new Scanner(new File(filename));
            return s.nextLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }
}

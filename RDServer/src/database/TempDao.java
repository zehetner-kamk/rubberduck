package database;


import database.domainObjects.TempDO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TempDao {
    private static final String TABLE_NAME = "temp";

    public boolean insert(TempDO temp) {
        String insertStatement = "INSERT INTO " + TABLE_NAME + " (duckid, time, temp) VALUES (?, ?, ?)";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(insertStatement)) {
                statement.setInt(1, temp.getDuckID());
                statement.setTimestamp(2, new Timestamp(temp.getTime()));
                statement.setDouble(3, temp.getTemp());

                statement.execute();
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public List<TempDO> getByDuckID(int duckid) {
        String insertStatement = "SELECT time, temp FROM " + TABLE_NAME + " WHERE duckid=(?)";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(insertStatement)) {
                statement.setInt(1, duckid);

                ResultSet result = statement.executeQuery();
                List<TempDO> temps = new ArrayList<>();
                while (result.next()) {
                    TempDO t = new TempDO(duckid, result.getTimestamp(1).getTime(), result.getDouble(2));
                    temps.add(t);
                }

                return temps;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new ArrayList<>();
    }

}

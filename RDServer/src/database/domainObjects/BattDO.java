package database.domainObjects;

public class BattDO {
    int duckid;
    long time;
    double batt;


    public BattDO(int duckid, long time, double batt) {
        this.duckid = duckid;
        this.time = time;
        this.batt = batt;
    }

    public BattDO(int duckid, double batt) {
        this.duckid = duckid;
        this.time = System.currentTimeMillis();
        this.batt = batt;
    }


    public int getDuckID() {
        return duckid;
    }

    public void setDuckID(int duckid) {
        this.duckid = duckid;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getBatt() {
        return batt;
    }

    public void setBatt(double batt) {
        this.batt = batt;
    }
}

package database.domainObjects;

public class TempDO {
    int duckid;
    long time;
    double temp;


    public TempDO(int duckid, long time, double temp) {
        this.duckid = duckid;
        this.time = time;
        this.temp = temp;
    }

    public TempDO(int duckid, double temp) {
        this.duckid = duckid;
        this.time = System.currentTimeMillis();
        this.temp = temp;
    }


    public int getDuckID() {
        return duckid;
    }

    public void setDuckID(int duckid) {
        this.duckid = duckid;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }
}

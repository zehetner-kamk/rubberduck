package database.domainObjects;

public class DuckDO {
    int id;
    double temp1;
    double temp2;

    public DuckDO(int id) {
        this.id = id;
        this.temp1 = 20.0;
        this.temp2 = 25.0;
    }

    public DuckDO(int id, double temp1, double temp2) {
        this.id = id;
        this.temp1 = temp1;
        this.temp2 = temp2;
    }


    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public double getTemp1() {
        return temp1;
    }

    public void setTemp1(double temp1) {
        this.temp1 = temp1;
    }

    public double getTemp2() {
        return temp2;
    }

    public void setTemp2(double temp2) {
        this.temp2 = temp2;
    }
}

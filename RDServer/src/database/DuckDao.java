package database;


import database.domainObjects.DuckDO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DuckDao {
    private static final String TABLE_NAME = "duck";

    public boolean insert(DuckDO duck) {
        String insertStatement = "INSERT INTO " + TABLE_NAME + " (duckid, temp1, temp2) VALUES (?, ?, ?)";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(insertStatement)) {
                statement.setInt(1, duck.getID());
                statement.setDouble(2, duck.getTemp1());
                statement.setDouble(3, duck.getTemp2());

                statement.execute();
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public DuckDO getByID(int duckid) {
        String insertStatement = "SELECT * FROM " + TABLE_NAME + " WHERE duckid=(?)";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(insertStatement)) {
                statement.setInt(1, duckid);

                ResultSet result = statement.executeQuery();
                if (result.next()) {
                    DuckDO duck = new DuckDO(result.getInt(2), result.getDouble(3), result.getDouble(4));
                    return duck;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public List<Integer> getDuckIDs() {
        String insertStatement = "SELECT duckid FROM " + TABLE_NAME;

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(insertStatement)) {

                ResultSet result = statement.executeQuery();
                List<Integer> games = new ArrayList<>();
                while (result.next()) {
                    games.add(result.getInt(1));
                }

                return games;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new ArrayList<>();
    }

    public boolean update(DuckDO duck) {
        String updateStatement = "UPDATE " + TABLE_NAME + " SET temp1=?, temp2=? WHERE duckid=?";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(updateStatement)) {
                statement.setDouble(1, duck.getTemp1());
                statement.setDouble(2, duck.getTemp2());
                statement.setInt(3, duck.getID());

                return statement.executeUpdate() > 0; //invalid id -> 0 rows affected -> false
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }


}

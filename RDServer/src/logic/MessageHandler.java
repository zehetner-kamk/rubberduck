package logic;

import network.Message;

public interface MessageHandler {
    boolean handleMessage(Message m);
}

package logic.impl;

import database.domainObjects.DuckDO;
import logic.MessageHandler;
import logic.MessageHandlerAbstract;
import network.Message;

public class UpdateHandlerImpl extends MessageHandlerAbstract implements MessageHandler {
    @Override
    public boolean handleMessage(Message m) {
        //validate params
        if (parseInteger(m.getParams().get("id")) == -100 || parseDouble(m.getParams().get("temp1")) == -100.0 || parseDouble(m.getParams().get("temp2")) == -100.0)
            return false;

        //update temp values in db
        return duckDao.update(new DuckDO(parseInteger(m.getParams().get("id")), parseDouble(m.getParams().get("temp1")), parseDouble(m.getParams().get("temp2"))));
    }
}

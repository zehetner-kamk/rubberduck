package logic.impl;

import database.domainObjects.DuckDO;
import logic.MessageHandler;
import logic.MessageHandlerAbstract;
import network.Message;

public class FetchHandlerImpl extends MessageHandlerAbstract implements MessageHandler {
    @Override
    public boolean handleMessage(Message m) {
        //validate input data
        if (parseInteger(m.getParams().get("id")) == -100) return false;

        //retrieve duck and store temperature values in message
        DuckDO duck = duckDao.getByID(parseInteger(m.getParams().get("id")));
        if (duck == null) return false;

        m.getParams().put("temp1", duck.getTemp1() + "");
        m.getParams().put("temp2", duck.getTemp2() + "");

        return true;
    }
}

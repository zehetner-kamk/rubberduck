package logic.impl;

import logic.MessageHandler;
import network.Message;

public class GenericMessageHandlerImpl implements MessageHandler {
    private MessageHandler registrationHandler = new RegistrationHandlerImpl();
    private MessageHandler dataHandler = new DataHandlerImpl();
    private MessageHandler fetchHandler = new FetchHandlerImpl();
    private MessageHandler updateHandler=new UpdateHandlerImpl();

    @Override
    public boolean handleMessage(Message m) {
        switch (m.getType()) {
            case r:
                return registrationHandler.handleMessage(m);
            case d:
                return dataHandler.handleMessage(m);
            case f:
                return fetchHandler.handleMessage(m);
            case u:
                return updateHandler.handleMessage(m);
            default:
                return false;
        }
    }
}

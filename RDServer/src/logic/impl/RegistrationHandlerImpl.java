package logic.impl;

import database.domainObjects.DuckDO;
import logic.MessageHandler;
import logic.MessageHandlerAbstract;
import network.Message;

import java.util.List;
import java.util.Random;

public class RegistrationHandlerImpl extends MessageHandlerAbstract implements MessageHandler {
    private Random r = new Random();

    @Override
    public boolean handleMessage(Message m) {
        if (parseDouble(m.getParams().get("temp1")) == -100.0 || parseDouble(m.getParams().get("temp2")) == -100.0)
            return false;

        int duckid = 0;
        List<Integer> ids = duckDao.getDuckIDs();
        while (duckid == 0 || ids.contains(duckid)) { //create new unique duck id
            duckid = r.nextInt(1000000000);
        }

        DuckDO duck = new DuckDO(duckid, parseDouble(m.getParams().get("temp1")), parseDouble(m.getParams().get("temp2")));
        m.getParams().put("id", duckid + "");

        return duckDao.insert(duck);
    }
}

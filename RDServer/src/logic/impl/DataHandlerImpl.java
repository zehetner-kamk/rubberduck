package logic.impl;

import database.domainObjects.BattDO;
import database.domainObjects.TempDO;
import logic.MessageHandler;
import logic.MessageHandlerAbstract;
import network.Message;

public class DataHandlerImpl extends MessageHandlerAbstract implements MessageHandler {

    @Override
    public boolean handleMessage(Message m) {
        //validate params
        if (parseInteger(m.getParams().get("id")) == -100 || parseDouble(m.getParams().get("temp")) == -100.0 || parseDouble(m.getParams().get("batt")) == -100.0)
            return false;

        //insert values into database and return if process was successful
        return tempDao.insert(new TempDO(parseInteger(m.getParams().get("id")), parseDouble(m.getParams().get("temp")))) &&
                battDao.insert(new BattDO(parseInteger(m.getParams().get("id")), parseDouble(m.getParams().get("batt"))));
    }
}

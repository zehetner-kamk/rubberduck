package logic;

import database.BattDao;
import database.DuckDao;
import database.TempDao;

public abstract class MessageHandlerAbstract {
    protected DuckDao duckDao = new DuckDao();
    protected TempDao tempDao = new TempDao();
    protected BattDao battDao = new BattDao();

    protected int parseInteger(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return -100;
        }
    }

    protected double parseDouble(String s) {
        try {
            return Double.parseDouble(s);
        } catch (NumberFormatException e) {
            return -100.0;
        }
    }
}

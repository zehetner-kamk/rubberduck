package network;

import java.util.HashMap;

public class Message {
    private MsgT type;
    private HashMap<String, String> params;

    public enum MsgT {r, d, f, u} //register new duck, submit data, fetch temps, update temps

    public Message(MsgT type, HashMap<String, String> params) {
        this.type = type;
        this.params = params;
    }

    public MsgT getType() {
        return type;
    }

    public void setType(MsgT type) {
        this.type = type;
    }

    public HashMap<String, String> getParams() {
        ensureParamExistence();
        return params;
    }

    public void setParams(HashMap<String, String> params) {
        this.params = params;
    }


    private void ensureParamExistence() { //ensures that all params for a specific msg type exist, even if they only contain error values
        switch (type) {
            case r:
                params.putIfAbsent("temp1", "-100");
                params.putIfAbsent("temp2", "-100");
                break;
            case d:
                params.putIfAbsent("id", "-100");
                params.putIfAbsent("temp", "-100");
                params.putIfAbsent("batt", "-100");
                break;
            case f:
                params.putIfAbsent("id", "-100");
                break;
            case u:
                params.putIfAbsent("id", "-100");
                params.putIfAbsent("temp1", "-100");
                params.putIfAbsent("temp2", "-100");
                break;
                //add other message types here in the future
            default:
                break;
        }
    }
}

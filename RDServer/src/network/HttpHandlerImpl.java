package network;

import com.sun.net.httpserver.HttpExchange;
import logic.MessageHandler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

public class HttpHandlerImpl implements com.sun.net.httpserver.HttpHandler {
    private MessageHandler handler;

    public HttpHandlerImpl(MessageHandler handler) {
        this.handler = handler;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException { //gets called when http request is made to server
        switch (httpExchange.getRequestMethod()) { //delegates to specific handler methods
            case "GET":
                this.handleGet(httpExchange);
                break;
            case "POST":
                this.handlePost(httpExchange);
                break;
            default:
                break;
        }
        httpExchange.close();
    }

    private void handleGet(HttpExchange httpExchange) {
        HashMap<String, String> params = getParams(httpExchange.getRequestURI().toString());

        try {
            if (!params.containsKey("type")) { //website was opened in browser without parameters, documentation
                StringBuilder sb = new StringBuilder();
                Files.lines(Paths.get("../../documentation.html")).forEach(sb::append);
                String html = sb.toString();

                httpExchange.sendResponseHeaders(200, html.length());
                httpExchange.getResponseBody().write(html.getBytes());
                return;
            }

            switch (params.get("type")) {
                case "fetch": //duck fetches temp values: id
                    Message m = new Message(Message.MsgT.f, params);
                    if (handler.handleMessage(m)) {
                        String s = m.getParams().get("temp1") + "," + m.getParams().get("temp2");

                        httpExchange.sendResponseHeaders(200, s.length());
                        httpExchange.getResponseBody().write(s.getBytes());
                    } else {
                        httpExchange.sendResponseHeaders(400, 0);
                    }
                    break;
                //add other cases in the future
                default:
                    httpExchange.sendResponseHeaders(400, 0);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handlePost(HttpExchange httpExchange) {
        HashMap<String, String> params = getParams(httpExchange.getRequestURI().toString());

        try {
            if (!params.containsKey("type")) { //wrong request
                httpExchange.sendResponseHeaders(400, 0);
                return;
            }

            switch (params.get("type")) {
                case "register": //register new duck: id, temp1, temp2
                    Message m = new Message(Message.MsgT.r, params);
                    if (handler.handleMessage(new Message(Message.MsgT.r, params))) {
                        String s = m.getParams().get("id");

                        httpExchange.sendResponseHeaders(200, s.length());
                        httpExchange.getResponseBody().write(s.getBytes());
                    } else {
                        httpExchange.sendResponseHeaders(400, 0);
                    }
                    break;
                case "data": //duck uploads data: id, temp, batt
                    if (handler.handleMessage(new Message(Message.MsgT.d, params))) {
                        httpExchange.sendResponseHeaders(200, 0);
                    } else {
                        httpExchange.sendResponseHeaders(400, 0);
                    }
                    break;
                case "update": //someone updated temp values for duck: id, temp1, temp2
                    if (handler.handleMessage(new Message(Message.MsgT.u, params))) {
                        httpExchange.sendResponseHeaders(200, 0);
                    } else {
                        httpExchange.sendResponseHeaders(400, 0);
                    }
                    break;
                //add other cases here in the future
                default:
                    break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private HashMap<String, String> getParams(String url) { //parses url parameters
        if (!url.contains("?")) return new HashMap<>(); //no parameters in url

        String[] rawParams = url.split("\\?")[1].split("&");
        HashMap<String, String> params = new HashMap<>();

        for (String p : rawParams) {
            String[] kv = p.split("="); //key-value
            params.putIfAbsent(kv[0], kv[1]);
        }

        return params;
    }
}

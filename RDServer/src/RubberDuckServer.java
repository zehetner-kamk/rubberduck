import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import logic.MessageHandler;
import logic.impl.GenericMessageHandlerImpl;
import network.HttpHandlerImpl;
import sun.net.httpserver.HttpServerImpl;

import java.net.InetSocketAddress;

public class RubberDuckServer {
    private static final int SERVER_PORT = 30020;

    public static void main(String[] args) {
        System.out.println("starting ...");

        MessageHandler handler = new GenericMessageHandlerImpl();

        HttpServer httpServer = null;
        while (httpServer == null) {
            try {
                httpServer = HttpServerImpl.create(new InetSocketAddress(SERVER_PORT), 0);
                HttpContext context = httpServer.createContext("/");
                context.setHandler(new HttpHandlerImpl(handler));

                httpServer.start();
            } catch (Exception e) {
                e.printStackTrace();

                if (httpServer != null) { //stops and resets httpServer, so it starts itself again after 10 sec
                    httpServer.stop(0);
                    httpServer = null;
                }
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
        }
    }
}
